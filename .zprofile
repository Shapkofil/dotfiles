export TERM="st-256color"
export TERMINAL="st"
export EDITOR="nvim"
export VISUAL="emacsclient -c -a emacs"
export BROWSER="firefox"
export ZDOTDIR="$HOME/.config/zsh"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/bin"
export XDG_CACHE_HOME="$HOME/.cache"

export TEXMFHOME="$XDG_CONFIG_HOME/texmf"


if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.local/bin/statusbar" ] ;
  then PATH="$HOME/.local/bin/statusbar:$PATH"
fi
